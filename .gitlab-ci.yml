stages:
  - lint
  - build
  - test

shellcheck:
  stage: lint
  image: koalaman/shellcheck-alpine
  services: []
  script:
    - shellcheck --version
    - shellcheck scripts/sfdx_helpers.sh

hadolint:
  stage: lint
  image: hadolint/hadolint:latest-alpine
  services: []
  script:
    - hadolint --version
    - hadolint docker/alpine.Dockerfile
    - hadolint docker/ubuntu.Dockerfile

# With kaniko special privileges are not required to build docker images.
build:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:debug
    entrypoint: ['']
  rules:
    - if: $CI_COMMIT_TAG
  script:
    - TAG=$CI_BUILD_ID
    - '[ "$CI_COMMIT_TAG" ] && TAG=$CI_COMMIT_TAG'
    - SF_CICD_IMAGE_URL=$CI_REGISTRY_IMAGE:$TAG

    - mkdir -p /kaniko/.docker
    - REG_AUTH=$(printf '%s:%s' "$CI_REGISTRY_USER" "$CI_REGISTRY_PASSWORD" | base64 | tr -d '\n')
    - printf '{"auths":{"%s":{"auth":"%s"}}}' "$CI_REGISTRY" "$REG_AUTH" > /kaniko/.docker/config.json

    - 'echo "Building: $SF_CICD_IMAGE_URL"'
    - >-
      /kaniko/executor
      --context "$CI_PROJECT_DIR"
      --dockerfile "docker/${DOCKER_IMAGE_DISTRO:-alpine}.Dockerfile"
      --destination "$SF_CICD_IMAGE_URL"
      --target "${DOCKER_IMAGE_SIZE:-full}"

test:
  stage: test
  image: $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
  rules:
    - if: $CI_COMMIT_TAG
  dependencies:
    - build
  script:
    - du -sh /
    - ls -l /opt
    - node -v
    - npm -v
    - jq --version
    - sfdx -v
    - bash --version

    - source /opt/sfdx_helpers.sh

    - log 'Test installing the lwc-dev-server package.'
    - sfdx project:generate -n test_project --json
    - cd test_project
    - npm install --save-dev @salesforce/lwc-dev-server
    - cd ..

    - log 'Test running the test-suite of a SF example app.'
    - SF_APP_URL=https://github.com/trailheadapps/dreamhouse-lwc/archive/refs/heads/main.tar.gz
    - curl -sSL "$SF_APP_URL" | tar xz
    - cd dreamhouse-lwc-main
    - install_lwc_jest
    - test_lwc_jest
