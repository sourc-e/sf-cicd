# GitLab CI/CD for Salesforce

This project contains a fully configured CI pipeline that works with Salesforce DX projects following the [package development model](https://trailhead.salesforce.com/en/content/learn/modules/sfdx_dev_model).
You can include a copy of, or import, this project's .yml file in your own project.
For a quick start example, please refer to [sfdx/sfdx-project-template](https://gitlab.com/sfdx/sfdx-project-template).

# What It Does

The CI pipeline for Salesforce is configured to create a scratch org and run tests of your Salesforce app every time you make a commit.
If tests pass, then the CI pipeline will create a new unlocked package version. You then have the option to manually approve the change
and deploy the new package version into your sandbox and production environments.

![Completed pipeline](images/completed-pipeline.png)

There are 6 stages to this pipeline:

1. **Preliminary**: For unit tests or linting checks that can run in the context of a runner, without deployment to a scratch org. By default, this stage runs [Jest tests](https://developer.salesforce.com/docs/component-library/documentation/lwc/lwc.testing) for [Lightning Web Components](https://trailhead.salesforce.com/en/content/learn/trails/build-lightning-web-components), if they exist.
2. **Create Scratch Org**:
   - **Create Scratch Org**: First checks if a scratch org has been created previously for reuse, otherwise checks if you are within your daily scratch org limits and then creates a new scratch org and deploys the app metadata to it.
   - **Update Scratch Org**: Can be run manually to update the metadata on the current scratch org.
   - **Delete Scratch Org**: Can be run manually to remove and clean up leftover scratch orgs.
3. **Test Scratch Org**: Runs tests on the scratch org, such as Apex tests. At this point your team can conduct manual testing in the scratch org.
4. **Packaging**: Creates a new unlocked package version for deployment to scratch, sandbox and production orgs. Can run on all branches.
5. **Staging**:
   - **Deploy Sandbox Package**: Deploys to a pre-production sandbox. This is a manually triggered job and only runs on the default/configured branch. See `AUTO_DEPLOY` for automatic deployment.
   - **Deploy Sandbox Metadata**: Deploys the metadata instead of a package.
   - **Deploy Scratch Package**: Deploys the package to the scratch org of the current branch.
6. **Production**:
   - **Deploy Production Package**: Promotes the app and deploys to production. This is a manually triggered job and only runs on the default/configured branch. See `AUTO_DEPLOY` for automatic deployment.
   - **Deploy Production Metadata**: Deploys the metadata instead of a package.

# Setting Up Your Project

1. Obtain a [Dev Hub](https://trailhead.salesforce.com/content/learn/projects/quick-start-salesforce-dx?trail_id=sfdx_get_started) org, which is required for creating scratch orgs in the CI pipeline. For testing, you can use a free [Trailhead Playground](https://trailhead.salesforce.com/content/learn/modules/trailhead_playground_management?trail_id=learn_salesforce_with_trailhead) or free [Developer Edition](https://developer.salesforce.com/signup) org.
2. In your Dev Hub org, enable both features [Dev Hub](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_enable_devhub.htm) and [Second-Generation Packaging](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_enable_secondgen_pkg.htm).
3. [Create](#create-an-unlocked-package) an unlocked package.
4. [Create](https://docs.gitlab.com/ee/gitlab-basics/create-project.html) a new GitLab project or [import](https://docs.gitlab.com/ee/user/project/import/repo_by_url.html) our quick start [sfdx/sfdx-project-template](https://gitlab.com/sfdx/sfdx-project-template).
5. [Configure](#configure-environment-variables) CI/CD environment variables in your GitLab project.
6. [Clone](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html) your project locally.
7. Add your Salesforce DX source to your local GitLab project, then commit and push the changes to initiate the CI pipeline.

# Create an Unlocked Package

The package stage of the CI pipeline will automatically create new package versions. You can manually click a button in the pipeline to install the packages into downstream Salesforce environments. Before running the pipeline, you need to have created the package definition and update your `sfdx-project.json` file to reference it.

1. From your project directory, [create an unlocked package](https://trailhead.salesforce.com/en/content/learn/projects/quick-start-unlocked-packages) using the [sfdx project:generate](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_dev2gp_create_pkg.htm) command. You only need to do this once per package/project.

2. Ensure your `sfdx-project.json` file mentions the package name and initial version number scheme in the `packageDirectories` property, and that the package name and ID (0Ho) are defined in the `packageAliases` property.

   An example `sfdx-project.json` file:

   ```json
   {
     "packageDirectories": [
       {
         "path": "force-app",
         "default": true,
         "package": "YOUR_PACKAGE_NAME",
         "versionNumber": "1.0.0.NEXT"
       }
     ],
     "namespace": "",
     "sourceApiVersion": "46.0",
     "packageAliases": {
       "YOUR_PACKAGE_NAME": "0HoXXXXXXXXXXXXXXX"
     }
   }
   ```

Once set, you don't have to manage the `versionNumber` property in `sfdx-project.json`. The CI pipeline determines the next appropriate version number automatically. Once a package version number has been released, the CI pipeline will create new package versions with a minor version number one greater than the latest released version.

For example, if the latest released version number is `1.2.0.5`, then the next time the CI pipeline creates a new package version then it'll automatically compute the new version number to be `1.3.0.1`.

# Configure Environment Variables

To use GitLab CI, you need to authenticate the GitLab project to your orgs in Salesforce.

1. Install [Salesforce CLI](https://developer.salesforce.com/tools/sfdxcli) on your local machine.
2. [Authenticate using the web-based oauth flow](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_web_flow.htm) to each of the orgs that represent your Dev Hub, sandbox, and production org. **Note, for initial testing of GitLab CI, your "sandbox" and "production" orgs can be Trailhead Playgrounds or any Developer Edition org.**
3. Use the `sfdx org:display --target-org <username> --verbose` command to get the [Sfdx Auth Url](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_auth_view_info.htm) for your various orgs. The URL you're looking for starts with `force://`. **Note, you must use the `--verbose` argument to see the `Sfdx Auth URL`.**
4. Populate the variables under **Settings > CI/CD > Variables** in your project in GitLab.

![Enter CI/CD variables](images/cicd-variables.png)

Here are the `Auth URL` variables to set:

- `DEVHUB_AUTH_URL`
- `PRODUCTION_AUTH_URL`\
  Note: can be omitted and defaults to `DEVHUB_AUTH_URL`.
- `SANDBOX_AUTH_URL_${BRANCH_NAME_SLUG}`\
  Deploy to different sandboxes depending on the branch name. E.g.: the branch `staging` will be deployed to the sandbox by the given `Auth URL` in `SANDBOX_AUTH_URL_staging`. Requires: `SANDBOX_BRANCHES_REGEX`.\
  *NB: This feature requires at least GitLab version 15.1 for [variable regular expressions](https://docs.gitlab.com/ee/ci/jobs/job_control.html#store-the-regex-pattern-in-a-variable).*

## Optional Variables

Configure your pipeline further with these **optional** variables:

- `DEPLOY_SCRATCH_ON_EVERY_COMMIT`: When `true`, new code is pushed to the scratch org on every commit on the same branch (default: `false`).
- `PACKAGE_NAME`: Must match one of the `packageDirectories` entries in `sfdx-project.json`. If not present, then the CI pipeline uses the default package directory from `sfdx-project.json`.
- `DEPLOYMENT_METHOD`: Either `PACKAGE` or `METADATA` (default: `PACKAGE`). Pipelines with the `PACKAGE` method will create a versioned, unlocked package and deploy it into the respective Salesforce orgs. If you would like to only push application meta-data in the `force-app/` directory and not create an unlocked package, then set the value to `METADATA`.
- `AUTO_DEPLOY`: To deploy automatically set to `scratch`, `sandbox`, `production` or a combination thereof (default: *empty*). Separate multiple values by spaces or commas.
- `ALWAYS_REUSE_SCRATCH_ORG`: Always attempt to reuse the scratch org on every branch including production and sandbox when `true` (default: `false`).
- `SCRATCH_ORG_LIFESPAN`: The lifespan of the scratch org in days (default: `7`, min: `1`, max: `30`).
- `INCREMENT_RELEASED_VERSION`: Increment the latest released version by `0.1.0` (default: `false`). Set to `true` only when a package version has been installed to the production org beforehand. Otherwise the package `"versionNumber"` defined in sfdx-project.json is used.
- `VERSION_STATUS_POLL_INTERVAL`: The polling interval in seconds for checking a package version's creation status (default: `45`).
- `VERSION_STATUS_POLL_TIMEOUT`: Timeout polling after `n` minutes (default: *empty*). The GitLab job normally times out after 1h.
- `SANDBOX_BRANCHES_REGEX`: Regular expression of branch names to be deployed to sandboxes (default: *empty*). Recommended to use characters `[a-zA-Z0-9_]` as branch names only. E.g.: `/^(staging|testing)$$/` (NB: the `$$` is for escaping to get one `$`.)
- `SANDBOX_BRANCH`: Define a single sandbox branch, if variable regular expressions are not supported by your GitLab instance.
- `PRODUCTION_BRANCH`: Define your production branch if it differs from the repository's default branch.
- `PRODUCTION_ENV_URL`: Define a login URL for the production environment (default: `https://login.salesforce.com`).
- `INSTALL_DEPENDENCIES`: When deploying to the sandbox or production, installs the dependencies defined in the default package under its `dependencies` key (default: `false`). For scratch orgs it's necessary to always install the dependencies automatically.
- `DOCKER_IMAGE_DISTRO`: Which distribution to use, `alpine` or `ubuntu` (default: `alpine`).
- `DOCKER_IMAGE_SIZE`: Set to `full` or `slim` (default: `full`). The full image contains a Java JRE useful for code linting.
- `SCRATCH_ORG_NAME`: The name to set for a scratch org in its `ScratchOrgInfo` (default: `$CI_PROJECT_PATH_SLUG/$CI_COMMIT_REF_SLUG`).
- `SCRATCH_ENV_NAME`: The name to use for a scratch org under "Deployments > Environments" (default: `scratch/$CI_COMMIT_REF_SLUG`). Note that using a `/` will create a folder structure.
- `SANDBOX_ENV_NAME`: The name to use for a sandbox org under "Deployments > Environments" (default: `sandbox/$CI_COMMIT_REF_SLUG`).
- `SHELLCHECK_ENABLED`: Enable `shellcheck` linting for checking all shell scripts in your project (default: `false`).

## Disable Jobs

To disable entire jobs, set the respective boolean variables to `true`:

- `TESTS_DISABLED`
- `LINTING_DISABLED`
- `SCRATCH_DISABLED`
- `SANDBOX_DISABLED`
- `PRODUCTION_DISABLED`

By default all scratch org jobs are run automatically in a merge request.
To disable them, set the respective boolean variables to `false`.

- `MR_TEST_LWC_ENABLED`
- `MR_LINTING_ENABLED`
- `MR_CREATE_SCRATCH_ORG_ENABLED`
- `MR_TEST_APEX_ENABLED`
- `MR_DEPLOY_SCRATCH_ORG_ENABLED`

# Example Project Setup

Use this minimal `.gitlab-ci.yml` if you have cloned this repository to `salesforce/sf-cicd`
on your GitLab instance. Set `SF_CICD_BRANCH` to the right branch (should be `main` usually).

```yaml
include:
  - project: salesforce/sf-cicd
    ref: $SF_CICD_BRANCH
    file: /Salesforce.gitlab-ci.yml

stages:
  - preliminary
  - create-scratch-org
  - test-scratch-org
  - packaging
  - staging
  - production
```

# Tips

Change your `config/project-scratch-def.json` so that scratch org logins don't expire, allowing Environment URLs to always work as long as the scratch org is alive:

```json
{
  "settings": {
    "securitySettings": {
      "enableAdminLoginAsAnyUser": true,
      "passwordPolicies": {
        "expiration": "Never",
        "lockoutInterval": "Forever"
      },
      "sessionSettings": {
        "disableTimeoutWarning": true,
        "forceLogoutOnSessionTimeout": false,
        "forceRelogin": false
      }
    }
  }
}
```

## Package vs. MDAPI

Packages are a collection of components and other metadata installed on or shared between different Salesforce orgs.

MDAPI offers a method to interact directly with Salesforce metadata.

**Advantages** of packages:

* Can be used to create apps that can be installed and shared with other orgs and the marketplace.
* Can automatically delete obsolete components or fields from older versions.
* Encourages modular software development where distinct packages solve different problems and meet particular business needs.

**Disadvantages** of packages:

* Cannot be uninstalled easily while leaving the data intact. Data is always destroyed and must be restored from backups.
* Some metadata cannot be deployed using a package. See *[Metadata Coverage](https://developer.salesforce.com/docs/metadata-coverage/56)*.
* Doesn't support splitting objects across different project sub-folders. E.g.: `Opportunity` in `force-app/project-one/objects/Opportunity` and `force-app/project-two/objects/Opportunity`

**Advantages** of MDAPI:

* Can deploy any type of metadata. See *[Metadata Coverage](https://developer.salesforce.com/docs/metadata-coverage/56)*.
* Can deploy directly using `sfdx force:source:deploy`. No need for setting up a package, creating versions and promoting them before installation.
* Supports splitting objects across different project sub-folders.

**Disadvantages** of MDAPI:

* Does not automatically delete obsolete components or fields. So-called "*[Destructive Changes](https://developer.salesforce.com/docs/atlas.en-us.daas.meta/daas/daas_destructive_changes.htm)*" are necessary.
* Does not store information about the source the deployed metadata came from.
